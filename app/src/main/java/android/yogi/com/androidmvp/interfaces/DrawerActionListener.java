package android.yogi.com.androidmvp.interfaces;

import android.yogi.com.androidmvp.constants.Constants;

/**
 * Created by yogesh.kalwar on 12/10/2016.
 */
public interface DrawerActionListener {
    void onDrawerItemSelected(Constants.NavigationAction action);
}
