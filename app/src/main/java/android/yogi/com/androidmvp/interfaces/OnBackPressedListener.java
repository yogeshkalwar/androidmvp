package android.yogi.com.androidmvp.interfaces;

/**
 * Created by yogesh.kalwar on 12/10/2016.
 */
public interface OnBackPressedListener {
    public boolean onBackPressed(boolean isHomeBackPressed);
}
