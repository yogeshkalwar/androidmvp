package android.yogi.com.androidmvp.ui.fragments;

import android.support.v4.app.Fragment;
import android.yogi.com.androidmvp.animations.BaseAnimation;

/**
 * Created by yogesh.kalwar on 12/10/2016.
 */
public class BaseFragment extends Fragment {
    private BaseAnimation _customAnimation = BaseAnimation.DEFAULT;

    public BaseAnimation getAnimation(){
        return _customAnimation;
    }

    protected void setAnimation(final BaseAnimation animation){
        this._customAnimation = new BaseAnimation(animation);
    }
}
