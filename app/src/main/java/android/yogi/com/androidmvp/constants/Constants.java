package android.yogi.com.androidmvp.constants;

/**
 * Created by yogesh.kalwar on 12/10/2016.
 */
public interface Constants {
    public enum NavigationAction{
        HOME,
        ABOUT_US
    }
}
